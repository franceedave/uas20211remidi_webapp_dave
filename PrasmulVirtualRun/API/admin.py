from django.contrib import admin
from API import models

admin.site.register(models.Athlete)
admin.site.register(models.Activity)
admin.site.register(models.Credential)