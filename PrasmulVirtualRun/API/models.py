from django.db import models

# Create your models here.

class Athlete(models.Model):
    Username = models.CharField(max_length=25,null=True)
    first_name = models.CharField(max_length=25,null=True)
    last_name = models.CharField(max_length=25,null=True)
    client_id = models.IntegerField(null=True)
    client_secret = models.CharField(max_length=25,null=True)
    refresh_token = models.CharField(max_length=25,null=True)
    access_token = models.CharField(max_length=25,null=True)
    Gender= models.CharField(max_length=10,null=True)
    city = models.CharField(max_length=30,null=True)
    


class Activity(models.Model):
    Activity_id = models.IntegerField(primary_key=True,default=0)
    ActivityChoices = (
        ('Walk', 'Walk'),
        ('Sprint', 'Sprint'),
        ('Jogging', 'Jogging'),
    )
    Activity = models.CharField(max_length=255, choices=ActivityChoices)
    
    def __str__(self):
        return self.Activity

    Distance = models.IntegerField(null=True)
    moving_time = models.IntegerField(null=True)
    elapse_time = models.IntegerField(null=True)
    


class Credential(models.Model):
    Athlete = models.CharField(max_length=25,null=True)
    value = models.CharField(max_length=200)

    # Here we override the save method to avoid that each user request create new credentials on top of the existing one
    
    def __str__(self):
        return f"{self.Athlete.Username} - {self.value}"

